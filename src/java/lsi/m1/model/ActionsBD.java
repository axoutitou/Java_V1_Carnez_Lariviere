package lsi.m1.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import static lsi.m1.utils.Constantes.*;

/**
 *
 * @author JAA
 */
public class ActionsBD {

	 private Connection conn;
	 private Statement stmt;
	 private PreparedStatement preparedStmt;
	 private ResultSet rs;
	 private UtilisateurBean user;
	 private ArrayList<UtilisateurBean> listeUsers;
	 private ArrayList<EmployeBean> listeEmployes;


	 public ActionsBD() {
		 try {
			 conn = DriverManager.getConnection(URL, USER_BDD, MDP_BDD);
		 } catch (SQLException e) {
			 System.out.println(e.getMessage());
		 }

	 }

	 public Statement getStatement() {
		 try {
			 stmt = conn.createStatement();
		 } catch (SQLException e) {
			 System.out.println(e.getMessage());
		 }
		 return stmt;
	 }
	 
	 public PreparedStatement getPreparedStatement(String req) {
		 try {
			 preparedStmt = conn.prepareStatement(req);
		 } catch (SQLException e) {
			 System.out.println(e.getMessage());
		 }
		 return preparedStmt;
	 }

	 public ResultSet getResultSet(String req) {
		 try {
			 stmt = getStatement();

			 rs = stmt.executeQuery(req);
		 } catch (SQLException e) {
			 System.out.println(e.getMessage());
		 }
		 return rs;
	 }

	 public ArrayList getUtilisateurs() {
		 listeUsers = new ArrayList<>();
		 try {
			 rs = getResultSet(REQ_UTILISATEURS);

			 while (rs.next()) {
				 UtilisateurBean userBean = new UtilisateurBean();
				 userBean.setLogin(rs.getString("LOGIN"));
				 userBean.setPassword(rs.getString("PASSWORD"));

				 listeUsers.add(userBean);
			 }
		 } catch (SQLException e) {
			 System.out.println(e.getMessage());
		 }
		 return listeUsers;
	 }


	 public ArrayList getEmployes() {
		 listeEmployes = new ArrayList<>();
		 try {
			 rs = getResultSet(REQ_TOUS_EMPLOYES);

			 while (rs.next()) {
		   EmployeBean emplBean = new EmployeBean();
		   emplBean.setId(rs.getString("ID"));
		   emplBean.setNom(rs.getString("NOM"));
		   emplBean.setPrenom(rs.getString("PRENOM"));
		   emplBean.setTelDom(rs.getString("TELDOM"));
		   emplBean.setTelPort(rs.getString("TELPORT"));
		   emplBean.setTelPro(rs.getString("TELPRO"));
		   emplBean.setAdresse(rs.getString("ADRESSE"));
		   emplBean.setCodePostal(rs.getString("CODEPOSTAL"));
		   emplBean.setVille(rs.getString("VILLE"));
		   emplBean.setEmail(rs.getString("EMAIL"));

		   listeEmployes.add(emplBean);
			 }
		 } catch (SQLException e) {
			 System.out.println(e.getMessage());
		 }
		 return listeEmployes;
	 }

	 public boolean verifInfosConnexion(UtilisateurBean userInput) {
		 boolean connexion = false;

		 listeUsers = getUtilisateurs();
		 for (UtilisateurBean userBase : listeUsers) {
			 if (userBase.getLogin().equals(userInput.getLogin()) && userBase.getPassword().equals(userInput.getPassword())) {
				 connexion = true;
			 }
		 }

		 return connexion;
	 }
	 
	 public void register(EmployeBean employe){
		  try {
			   preparedStmt = getPreparedStatement(REQ_INSERT_EMPLOYE);
			   preparedStmt.setString(1, employe.getNom());
			   preparedStmt.setString(2, employe.getPrenom());
			   preparedStmt.setString(3, employe.getTelDom());
			   preparedStmt.setString(4, employe.getTelPort());
			   preparedStmt.setString(5, employe.getTelPro());
			   preparedStmt.setString(6, employe.getAdresse());
			   preparedStmt.setString(7,employe.getCodePostal());
			   preparedStmt.setString(8, employe.getVille());
			   preparedStmt.setString(9, employe.getEmail());
			   preparedStmt.executeUpdate();
		  } catch (SQLException ex) {
			   Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
		  }
	 }
	 
	 public void update(EmployeBean employe){
		  try {
			   preparedStmt = getPreparedStatement(REQ_UPDATE_EMPLOYE);
			   preparedStmt.setString(1, employe.getNom());
			   preparedStmt.setString(2, employe.getPrenom());
			   preparedStmt.setString(3, employe.getTelDom());
			   preparedStmt.setString(4, employe.getTelPort());
			   preparedStmt.setString(5, employe.getTelPro());
			   preparedStmt.setString(6, employe.getAdresse());
			   preparedStmt.setString(7,employe.getCodePostal());
			   preparedStmt.setString(8, employe.getVille());
			   preparedStmt.setString(9, employe.getEmail());
			   preparedStmt.setString(10, employe.getId());
			   preparedStmt.executeUpdate();
		  } catch (SQLException ex) {
			   Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
		  }
	 }
	 
	 public void delete(String paramSelectId){
		  try {
			   preparedStmt = getPreparedStatement(REQ_DELETE_EMPLOYE);
			   preparedStmt.setString(1, paramSelectId);
			   preparedStmt.executeUpdate();
		  } catch (SQLException ex) {
			   Logger.getLogger(ActionsBD.class.getName()).log(Level.SEVERE, null, ex);
		  }
	 }

	 public EmployeBean getEmployeById(String paramId){
		  
		 listeEmployes = getEmployes();
		 for (EmployeBean employe : listeEmployes) {
			if(employe.getId().equals(paramId)) return employe;
		 }
		 return null;
	 }
}
