/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lsi.m1.ctrl;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lsi.m1.model.ActionsBD;
import lsi.m1.model.EmployeBean;
import lsi.m1.model.UtilisateurBean;
import static lsi.m1.utils.Constantes.*;


/**
 *
 * @author Axel Carnez
 */
public class Controleur extends HttpServlet {
	 HttpSession session;
	 /**
	  * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	  * methods.
	  *
	  * @param request servlet request
	  * @param response servlet response
	  * @throws ServletException if a servlet-specific error occurs
	  * @throws IOException if an I/O error occurs
	  */
	 protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  
		  if(request.getParameter(FRM_BTN_CONNEXION) != null){
			   UtilisateurBean userInput = new UtilisateurBean();
	
			   if(request.getParameter(FRM_LOGIN).length() == 0 ||  request.getParameter(FRM_MDP).length() == 0){
					request.setAttribute("error", ERR_EMPTY_FIELD);
					request.getRequestDispatcher(JSP_ACCUEIL).forward(request, response);
			   }
			   else{
					userInput.setLogin(request.getParameter(FRM_LOGIN));
					userInput.setPassword(request.getParameter(FRM_MDP));
					ActionsBD actionsBD = new ActionsBD();
					if (actionsBD.verifInfosConnexion(userInput)) {
						session = request.getSession();
						session.setAttribute("user", userInput);
						request.setAttribute("listEmployes", actionsBD.getEmployes());
						request.getRequestDispatcher(JSP_USERS).forward(request, response);
					} else {
						request.setAttribute("error", ERR_WRONG_LOGIN);
						request.getRequestDispatcher(JSP_ACCUEIL).forward(request, response);
					}	
			   }
			   
		  }
		  else if(request.getParameter(FRM_BTN_SHOW_AJOUT) != null){
			   request.getRequestDispatcher(JSP_NEW).forward(request, response);
		  }
		  else if(request.getParameter(FRM_BTN_SHOW_DETAILS) != null){
			   ActionsBD actionsBD = new ActionsBD();
			   String selectId = request.getParameter(FRM_SELECTED_EMPLOYE);
			   if(selectId == null){
					request.setAttribute("listEmployes", actionsBD.getEmployes());
					request.setAttribute("error", ERR_EMPTY_DETAILS);
					request.getRequestDispatcher(JSP_USERS).forward(request, response);
			   }
			   else{
					request.setAttribute("employe", actionsBD.getEmployeById(selectId));
					request.getRequestDispatcher(JSP_NEW).forward(request, response);	   
			   }
		  }
		  else if(request.getParameter(FRM_BTN_SHOW_LISTE) != null){
			   ActionsBD actionsBD = new ActionsBD();
			   request.setAttribute("listEmployes", actionsBD.getEmployes());
			   request.getRequestDispatcher(JSP_USERS).forward(request, response);
		  }
		  else if(request.getParameter(FRM_BTN_AJOUTER) != null || request.getParameter(FRM_BTN_MODIFIER) != null){
			   ActionsBD actionsBD = new ActionsBD();
			   EmployeBean employe = new EmployeBean();
			   employe.setId(request.getParameter(FRM_ID));
			   employe.setNom(request.getParameter(FRM_NOM));
			   employe.setPrenom(request.getParameter(FRM_PRENOM));
			   employe.setTelDom(request.getParameter(FRM_TEL_DOM));
			   employe.setTelPort(request.getParameter(FRM_TEL_PORT));
			   employe.setTelPro(request.getParameter(FRM_TEL_PRO));
			   employe.setAdresse(request.getParameter(FRM_ADRESSE));
			   employe.setCodePostal(request.getParameter(FRM_CODE_POSTAL));
			   employe.setVille(request.getParameter(FRM_VILLE));
			   employe.setEmail(request.getParameter(FRM_EMAIL));
			   
			   if(request.getParameter(FRM_BTN_AJOUTER) != null){
					actionsBD.register(employe);
					request.setAttribute("success", SUC_AJOUT);
			   }
			   else{
					actionsBD.update(employe);
					request.setAttribute("success", SUC_MODIF);
			   }
			   request.setAttribute("listEmployes", actionsBD.getEmployes());
			   request.getRequestDispatcher(JSP_USERS).forward(request, response);
		  }
		  else if(request.getParameter(FRM_BTN_SUPPRIMER) != null){
			   ActionsBD actionsBD = new ActionsBD();
			   String selectId = request.getParameter(FRM_SELECTED_EMPLOYE);
			   if(selectId == null){
					request.setAttribute("listEmployes", actionsBD.getEmployes());
					request.setAttribute("error", ERR_EMPTY_SUPPR);
					request.getRequestDispatcher(JSP_USERS).forward(request, response);
			   }
			   else{
					actionsBD.delete(selectId);
					request.setAttribute("listEmployes", actionsBD.getEmployes());
					request.setAttribute("success", SUC_SUPPR);
					request.getRequestDispatcher(JSP_USERS).forward(request, response);	
			   }	  
		  }
		  else if(request.getParameter(FRM_BTN_DECONNEXION) != null){
			   session = request.getSession();
			   session.removeAttribute("user");
			   request.getRequestDispatcher(JSP_GOODBYE).forward(request, response);
		  }
		  else{
			   request.getRequestDispatcher(JSP_ACCUEIL).forward(request, response);
		  }
	 }	  
	 

	 // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	 /**
	  * Handles the HTTP <code>GET</code> method.
	  *
	  * @param request servlet request
	  * @param response servlet response
	  * @throws ServletException if a servlet-specific error occurs
	  * @throws IOException if an I/O error occurs
	  */
	 @Override
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
			 throws ServletException, IOException {
		  processRequest(request, response);
	 }

	 /**
	  * Handles the HTTP <code>POST</code> method.
	  *
	  * @param request servlet request
	  * @param response servlet response
	  * @throws ServletException if a servlet-specific error occurs
	  * @throws IOException if an I/O error occurs
	  */
	 @Override
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
			 throws ServletException, IOException {
		  processRequest(request, response);
	 }

	 /**
	  * Returns a short description of the servlet.
	  *
	  * @return a String containing servlet description
	  */
	 @Override
	 public String getServletInfo() {
		  return "Short description";
	 }// </editor-fold>

}
