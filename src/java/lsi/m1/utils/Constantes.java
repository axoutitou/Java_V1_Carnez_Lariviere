/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lsi.m1.utils;

/**
 *
 * @author nitsu
 */
public class Constantes {
	 // BASE DE DONNEES
	 public static final String URL = "jdbc:derby://localhost:1527/JEEPRJ";
	 public static final String USER_BDD = "jee";
	 public static final String MDP_BDD = "jee";
	 public static final String REQ_UTILISATEURS = "SELECT * from UTILISATEUR";
	 public static final String REQ_TOUS_EMPLOYES = "SELECT * from EMPLOYES";
	 public static final String REQ_INSERT_EMPLOYE = "INSERT INTO EMPLOYES (NOM, PRENOM, TELDOM, TELPORT, TELPRO, ADRESSE, CODEPOSTAL, VILLE, EMAIL) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	 public static final String REQ_UPDATE_EMPLOYE = "UPDATE EMPLOYES SET NOM=?, PRENOM=?, TELDOM=?, TELPORT=?, TELPRO=?, ADRESSE=?, CODEPOSTAL=?, VILLE=?, EMAIL=? WHERE ID=?";
	 public static final String REQ_DELETE_EMPLOYE = "DELETE FROM EMPLOYES WHERE ID=?";
	 
	 
	 // PAGES
	 public static final String JSP_ACCUEIL =  "WEB-INF/index.jsp";
	 public static final String JSP_USERS = "WEB-INF/home.jsp";
	 public static final String JSP_NEW = "WEB-INF/new.jsp";
	 public static final String JSP_GOODBYE = "WEB-INF/goodbye.jsp";
	 
	 // CHAMPS FORMULAIRES
	 public static final String FRM_LOGIN = "login";
	 public static final String FRM_MDP = "password";
	 
	 public static final String FRM_SELECTED_EMPLOYE = "employeSel";	
	 
	 public static final String FRM_ID = "id";
	 public static final String FRM_NOM = "nom";
	 public static final String FRM_PRENOM = "prenom";
	 public static final String FRM_TEL_DOM = "telDom";
	 public static final String FRM_TEL_PORT = "telPort";
	 public static final String FRM_TEL_PRO = "telPro";
	 public static final String FRM_ADRESSE = "adresse";
	 public static final String FRM_CODE_POSTAL = "codePostal";
	 public static final String FRM_VILLE = "ville";
	 public static final String FRM_EMAIL = "email";

	 
	 // BOUTON ACTIONS
	 public static final String FRM_BTN_SHOW_AJOUT = "btnShowAjout";
	 public static final String FRM_BTN_SHOW_LISTE = "btnShowListe";
	 public static final String FRM_BTN_SHOW_DETAILS = "btnShowDetails";
	 public static final String FRM_BTN_AJOUTER = "btnAjouter";
	 public static final String FRM_BTN_MODIFIER = "btnModifier";
	 public static final String FRM_BTN_SUPPRIMER = "btnSuprimer";
	 public static final String FRM_BTN_CONNEXION = "btnLogin";
	 public static final String FRM_BTN_DECONNEXION = "btnDeconnexion";
	 
	 
	 // ERREURS
	 public static final String ERR_WRONG_LOGIN = "Echec de la connexion! Vérifiez votre login et/ou mot de passe et essayer à nouveau";
	 public static final String ERR_EMPTY_FIELD = "Vous devez renseigner les deux champs";
	 public static final String ERR_EMPTY_SUPPR = "Veuillez sélectionner l'employé(e)à supprimer";
	 public static final String ERR_EMPTY_DETAILS = "Veuillez sélectionner l'employé(e)à afficher";
	 
	 // SUCCES
	 public static final String SUC_AJOUT = "Ajout réussi";
	 public static final String SUC_SUPPR = "Suppression réussi";
	 public static final String SUC_MODIF = "Modification réussi";
}
