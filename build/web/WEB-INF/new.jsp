<%-- 
    Document   : nouvelUtilisateur
    Created on : 24 oct. 2019, 11:22:59
    Author     : Axel Carnez
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     
    <!-- Style -->
    <style>
      .row{
        margin-bottom: 10px;
      }
    </style>

    <title>Java JEE- Projet Final</title>
  </head> 

  <body>
   <script>
       <c:if  test="${!empty employe}">          
        $(function() {
          $("#id").val("${employe.id}");
          $("#nom").val("${employe.nom}");
          $("#prenom").val("${employe.prenom}");
          $("#telDom").val("${employe.telDom}");
          $("#telPort").val("${employe.telPort}");
          $("#telPro").val("${employe.telPro}");
          $("#adresse").val("${employe.adresse}");
          $("#codePostal").val("${employe.codePostal}");
          $("#ville").val("${employe.ville}");
          $("#email").val("${employe.email}");
        });
    </c:if>
  </script>
    <div class="container-fluid">
        
    <div class="row">
        <div class="col-md-5">
          <h1>Ajout/Modification d'un employé</h1>
        </div>
        <div class="offset-md-2 col-md-3">
            <p style="float: left; margin-top: 3%">Bonjour ${user.login} ! Votre session est active</p>
            <form action="Controleur" method="POST">
                <button type="submit" class="btn btn-default" name="btnDeconnexion" style="margin-top:5px; background-color: white">
                   <i class="material-icons">power_settings_new</i>
                </button>
            </form>
        </div>
    </div>
            
      <div class="row justify-content-center">
        <div class="col-md-6">
          <div class="card">
            <div class="card-body"> 

                <form action="Controleur" method="POST">
                    <c:if  test="${!empty employe}">          
                        <input type="hidden" class="form-control" name="id" id="id" value="${employe.id}">
                   </c:if>
                   <c:if  test="${empty employe}">          
                        <input type="hidden" class="form-control" name="id" id="id">
                   </c:if>
                    <div class="row">
                      <div class="col-md-2">
                        <label for="nom">Nom</label>
                      </div>
                      <div class="col-md-10">
                           <c:if  test="${!empty employe}">          
                                <input type="text" class="form-control" name="nom" id="nom" required="true" value="${employe.nom}">
                           </c:if>
                            <c:if  test="${empty employe}">          
                                <input type="text" class="form-control" name="nom" id="nom" required="true">
                           </c:if>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-2">
                        <label for="Prenom">Prénom</label>
                      </div>
                      <div class="col-md-10">
                            <c:if  test="${!empty employe}">          
                                <input type="text" class="form-control" name="prenom" id="prenom" required="true" value="${employe.prenom}">
                           </c:if>
                            <c:if  test="${empty employe}">          
                                <input type="text" class="form-control" name="prenom" id="prenom" required="true">
                           </c:if>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-2">
                          <label for="telDom">Tél dom</label>
                      </div>
                      <div class="col-md-10">
                          <c:if  test="${!empty employe}">          
                                <input type="text" class="form-control" name="telDom" id="telDom" required="true" value="${employe.telDom}">
                           </c:if>
                            <c:if  test="${empty employe}">          
                                <input type="text" class="form-control" name="telDom" id="telDom" required="true">
                           </c:if>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-2">
                        <label for="telPort">Tél mob</label>
                      </div>
                      <div class="col-md-10">
                            <c:if  test="${!empty employe}">          
                                <input type="text" class="form-control" name="telPort" id="telPort" required="true" value="${employe.telPort}">
                           </c:if>
                            <c:if  test="${empty employe}">          
                                <input type="text" class="form-control" name="telPort" id="telPort" required="true">
                           </c:if>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-2">
                        <label for="telPro">Tél pro</label>
                      </div>
                      <div class="col-md-10">
                            <c:if  test="${!empty employe}">          
                                <input type="text" class="form-control" name="telPro" id="telPro" required="true" value="${employe.telPro}">
                           </c:if>
                            <c:if  test="${empty employe}">          
                                <input type="text" class="form-control" name="telPro" id="telPro" required="true">
                           </c:if>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-2">
                        <label for="adresse">Adresse</label>
                      </div>
                      <div class="col-md-4">
                          <c:if  test="${!empty employe}">          
                                <input type="text" class="form-control" name="adresse" id="adresse" required="true" value="${employe.adresse}">
                           </c:if>
                            <c:if  test="${empty employe}">          
                                <input type="text" class="form-control" name="adresse" id="adresse" required="true">
                           </c:if>
                      </div>
                        

                      <div class="col-md-2">
                        <label for="codePostal">Code Postal</label>
                      </div>
                      <div class="col-md-4">
                           <c:if  test="${!empty employe}">          
                                <input type="text" class="form-control" name="codePostal" id="codePostal" required="true" value="${employe.codePostal}">
                           </c:if>
                            <c:if  test="${empty employe}">          
                                <input type="text" class="form-control" name="codePostal" id="codePostal" required="true">
                           </c:if>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-2">
                        <label for="ville">Ville</label>
                      </div>
                      <div class="col-md-4">
                            <c:if  test="${!empty employe}">          
                                <input type="text" class="form-control" name="ville" id="ville" required="true" value="${employe.ville}">
                           </c:if>
                            <c:if  test="${empty employe}">          
                                <input type="text" class="form-control" name="ville" id="ville" required="true">
                           </c:if>
                      </div>
                        

                      <div class="col-md-2">
                        <label for="email">Adresse e-mail</label>
                      </div>
                      <div class="col-md-4">
                            <c:if  test="${!empty employe}">          
                                <input type="text" class="form-control" name="email" id="email" required="true" value="${employe.email}">
                           </c:if>
                            <c:if  test="${empty employe}">          
                                <input type="text" class="form-control" name="email" id="email" required="true">
                           </c:if>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="offset-md-8 col-md-4">
                        <c:if  test="${empty employe}">    
                            <button class="btn btn-primary" type="submit" name="btnAjouter">Valider</button>
                        </c:if>
                        <c:if  test="${!empty employe}">    
                            <button class="btn btn-primary" type="submit" name="btnModifier">Modifier</button>
                        </c:if>
                   </form>
                   <form action="Controleur" method="POST" style="float : right">
                    <button class="btn" type="submit" name="btnShowListe">Voir Liste</button>
                   </form>
                      </div>
                    </div>
                  

            </div>
          </div>
        </div>  
      </div>
    </div>
  </body>

</html>
