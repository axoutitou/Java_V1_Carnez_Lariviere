<%-- 
    Document   : home.jsp
    Created on : 24 oct. 2019, 09:43:52
    Author     : Axel Carnez
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
  <head>    
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

    <!-- DataTable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <!-- Style -->
    <style>
      .row{
        margin-bottom: 30px;
      }
    </style>
    
    <title>Java JEE- Projet Final</title>
  </head> 

  <body>
    <script>
      $(document).ready( function () {
        $('#table_id').DataTable({
          "paging":   false,
          "ordering": false,
          "info":     false,
          "searching": false
        });
      } );
    </script>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-5">
          <h1>Liste des employés</h1>
        </div>
        <div class="offset-md-2 col-md-3">
            <p style="float: left; margin-top: 3%">Bonjour ${user.login} ! Votre session est active</p>
            <form action="Controleur" method="POST">
                <button type="submit" class="btn btn-default" name="btnDeconnexion" style="margin-top:5px; background-color: white">
                   <i class="material-icons">power_settings_new</i>
                </button>
            </form>
        </div>
      </div>
    
    <form action="Controleur" method="POST">
      <div class="row justify-content-center">
        <div class="col-md-12">
            <c:if test="${fn:length(listEmployes) gt 0}">
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <th>Sél</th>
                        <th>NOM</th>
                        <th>PRENOM</th>
                        <th>TEL DOMICILE</th>
                        <th>TEL PORTABLE</th>
                        <th>TEL PRO</th>
                        <th>ADRESSE</th>
                        <th>CODE POSTAL</th>
                        <th>VILLE</th>
                        <th>EMAIL</th>
                    </tr>
                </thead>
                <tbody>
                    
                     <c:forEach items="${listEmployes}" var="employe">
                        <tr>
                            <td><input type="radio"  name="employeSel" value="${employe.id}"></td>
                            <td>${employe.nom}</td>
                            <td>${employe.prenom}</td>
                            <td>${employe.telDom}</td>
                            <td>${employe.telPort}</td>
                            <td>${employe.telPro}</td>
                            <td>${employe.adresse}</td>
                            <td>${employe.codePostal}</td>
                            <td>${employe.ville}</td>
                            <td>${employe.email}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
             </c:if>
            <c:if test="${fn:length(listEmployes) eq 0}">
                <p style="color: blue; text-align: center"> Nous devons recruter</p>
            </c:if>
        </div>  
      </div>

      <div class="row">
          <div class="col-md-5">
              <c:if  test="${user.login == 'admin'}">  
                <button type="submit" class="btn btn-primary" name="btnSuprimer">Supprimer</button>
                <button type="submit" class="btn btn-primary" name="btnShowDetails">Details</button>  
                <button type="submit" class="btn" name="btnShowAjout">Ajouter</button> 
               </c:if>
          </div>
          <div class="col-md-5">
              <c:if  test="${!empty success}">          
                  <p style="color:green">${success}</p>
               </c:if>
               <c:if  test="${!empty error}">          
                  <p style="color:red">${error}</p>
               </c:if>
          </div>
        </div>
    </form>
    </div>
  </div>
        
  </body>
</html>
